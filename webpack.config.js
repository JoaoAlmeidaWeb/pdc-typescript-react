const path = require("path");
const webpack = require("webpack");

const uglifyOptions = new webpack.optimize.UglifyJsPlugin({
    // Eliminate comments
    comments: false,

    // Compression specific options
    compress: {
        // remove warnings
        warnings: false,

        // Drop console statements
        drop_console: true
    },
})

const config = {
    /*
    * app.ts represents the entry point to your web application. Webpack will
    * recursively go through every "require" statement in app.ts and
    * efficiently build out the application's dependency tree.
    */
    entry: ["./src/PdcApp.tsx"],

    /*
    * The combination of path and filename tells Webpack what name to give to
    * the final bundled JavaScript file and where to store this file.
    */
    output: {
        path: path.resolve(__dirname, "build"),
        filename: "bundle.js"
    },

    watch: true,

    // configure the dev server to run
    devServer: {
        port: 3000,
        historyApiFallback: true,
        inline: true,
    },
    /*
    * resolve lets Webpack now in advance what file extensions you plan on
    * "require"ing into the web application, and allows you to drop them
    * in your code.
    */
    resolve: {
        extensions: [".ts", ".tsx", ".js", ".css", ".scss"]
    },

    module: {
        /*
        * Each loader needs an associated Regex test that goes through each
        * of the files you've included (or in this case, all files but the
        * ones in the excluded directories) and finds all files that pass
        * the test. Then it will apply the loader to that file. I haven't
        * installed ts-loader yet, but will do that shortly.
        */
        loaders: [
            {
                test: /\.scss$/,
                loaders: [
                    'style-loader',
                    'css-loader?modules',
                    'sass-loader'
                ]
            }, {
                test: /\.js$/,
                use: ['babel-loader', 'source-map-loader'],
                exclude: /node_modules/,
            }, {
                test: /\.tsx?$/,
                loader: "ts-loader",
                exclude: /node_modules/
            }
        ]
    },

    plugins: [
        uglifyOptions
    ]
};

module.exports = config;