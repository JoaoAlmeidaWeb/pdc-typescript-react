import * as React from 'react';
import { Button } from 'react-bootstrap';

interface LanguageFormProps {
    key: number
}

interface LanguageFormState {

}

export default class LanguageForm extends React.Component<any, any> {
    constructor(props:any){
        super(props);
    }

    render(){
        return (
            <div>
                <input type="text" placeholder="Idioma"/>
                <select name="experiencia" id="experiencia">
                    <option value="iniciado">iniciado</option>
                    <option value="intremedio">intremedio</option>
                    <option value="avancado">avancado</option>
                    <option value="expert">expert</option>
                </select>
                <button onClick={this.props.action} value={this.props.id}> - </button>
            </div>
        )
    }
}