import * as React from 'react';
import * as axios from 'axios';
import * as style from './SkillForm.scss';
import { Button } from 'react-bootstrap';
import {experienceItem} from '../PdcForm/PdcForm'
import {skillItem} from '../PdcForm/PdcForm'

interface SkillFormProps {
    key: number,
    action?: any,
    id?: any,
    item: skillItem,
    experienceLevels: experienceItem[],
    update: (event:any) => void;
    delete: (event:any) => void;
}

export default class SkillForm extends React.Component<SkillFormProps, null> {

    constructor (props:SkillFormProps) {
        super(props);
    }

    public componentDidMount () {}

    public handleUpdate (event:any) {
        let skillObj = this.props.item;
        let attribute : string = event.target.id.toString();
        skillObj[attribute] = event.target.value;
        this.props.update(skillObj);
    }

    render() {
        const experienceLevels = this.props.experienceLevels.map((exp: experienceItem,idx:number) =>
            <option key={idx} value={exp.value}>{exp.name}</option>
        );

        return (
            <div id={this.props.item.value.toString()}>
                <input id="name" onChange = { e => this.handleUpdate(e)} type="text" placeholder="Nome competencia" defaultValue={this.props.item.name}/>
                <select name="experiencia" id="experiencia" defaultValue={this.props.item.expId.toString()}>
                    {experienceLevels}
                </select>
                <input type="number"  onChange = { e => this.handleUpdate(e)} id="anosExp" value={this.props.item.year} placeholder="Anos Experiencia" />
                <button type="button" onClick={this.props.delete} value={this.props.item.value.toString()}> - </button>
            </div>
        )
    }
}