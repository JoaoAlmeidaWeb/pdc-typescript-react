import * as React from 'react';
import { Button } from 'react-bootstrap';


interface MultipleFormsProps {
    elementForm: any,
    elementData?: any
}

interface MultipleFormsState {
    testArray: number[]
}

export default class MultipleForms extends React.Component<MultipleFormsProps, MultipleFormsState> {
    constructor (props: MultipleFormsProps) {
        super(props);

        this.state = {
            testArray: this.props.elementData || []
        }

        this.addArray = this.addArray.bind(this);
        this.deleteArray = this.deleteArray.bind(this);
    }

    public addArray(event:any) {
        let arr = this.state.testArray;
        arr.push((arr[arr.length - 1] || 0) + 1);
        this.setState({testArray: arr});
    }

    public deleteArray(event:any){
        let arr = this.state.testArray;
        arr.splice(arr.indexOf(+event.target.value), 1);
        this.setState({testArray:arr});
    }

    render(){
        if (this.props.elementData) {
            const Child = this.props.elementForm;
            const forms = this.state.testArray.map((item: any) =>
                <Child action={this.deleteArray} key={item.value} item={item}/>
            );

            return(
                <div>
                    {forms}
                    <button onClick={this.addArray}>+</button>
                </div>
            )
        } else {
            return null
        }
    }
}