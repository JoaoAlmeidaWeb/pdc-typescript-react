import * as React from 'react';
import { Button } from 'react-bootstrap';

interface LocationCheckboxProps {
    key: number,
    item: object
}

interface LocationCheckboxState {

}

export default class LocationCheckbox extends React.Component<any,any> {
    constructor(props:any){
        super(props);
    }

    render(){
        const name = this.props.item.name;

        return (
            <div>
                <label>
                    {name}
                <input onClick={this.props.action} id={name} defaultChecked={this.props.item.checked} value={this.props.item.value} type="checkbox"/>
                </label>
            </div>
        )
    }
}