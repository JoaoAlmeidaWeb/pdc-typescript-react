import * as React from 'react';
import SkillForm from '../SkillForm/SkillForm'
import {experienceItem} from '../PdcForm/PdcForm'
import {skillItem} from '../PdcForm/PdcForm'

interface SkillFormsProps{
    skills : skillItem[],
    experienceLevels : experienceItem[],
    update : (event:any) => void;
    add : (event:any) => void;
    delete : (event:any) => void;
}


export default class SkillForms extends React.Component<SkillFormsProps, null> {
    constructor(props : SkillFormsProps){
        super(props);


    }
    public componentDidMount(){

    }


    render(){
        const forms = this.props.skills.map((item: skillItem) =>
            <SkillForm delete={this.props.delete} update={this.props.update}  key={item.value} item={item} experienceLevels={this.props.experienceLevels}/>
        );

        return(
            <div>
                {forms}
                <button type="button" onClick={this.props.add} >+</button>
            </div>
        )
    }

    
}