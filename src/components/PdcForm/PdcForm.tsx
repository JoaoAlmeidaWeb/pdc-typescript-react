// Remember to rename your file to PdcForm.tsx and
// place it within your src/ directory

import * as React from 'react';
import * as axios from 'axios';
import { Button } from 'react-bootstrap';
import LocationCheckbox from '../LocationCheckbox/LocationCheckbox';
import MultipleForms from '../MultipleForms/MultipleForms';
import LanguageForm from '../LanguageForm/LanguageForm';
import SkillForms from '../SkillForms/SkillForms';
import * as style from './PdcForm.scss';

interface PdcFormProps {

}

interface PdcFormState{
    name?: string,
    email?: string,
    contact?: string,
    description?: string,
    expectedWage?: number,
    locationDB: dispItem[],
    skillDB: skillItem[],
    experienceDB : experienceItem[]
}

export  interface skillItem {
    name: string,
    year: number,
    value: number,
    expId : number,

}

export  interface experienceItem{
    name : string,
    value : number
}
interface dispItem{
    name: string,
    value: number,
    checked: number
}



class PdcForm extends React.Component<PdcFormProps, PdcFormState> {
    constructor(props: PdcFormProps) {
        super(props);
        this.state = {
            locationDB: [],
            skillDB: [],
            experienceDB : []
        }

        this.updateCheckBox = this.updateCheckBox.bind(this);
        this.updateSkills = this.updateSkills.bind(this);
        this.addArray = this.addArray.bind(this);
        this.deleteArray = this.deleteArray.bind(this);
    }

    public componentDidMount() {
        // Retrieve location for checkboxes
        axios.default.request({
            method: 'GET',
            url: 'http://localhost:3001/Location'
        })
        .then(res => {
            this.setState({locationDB:res.data});
            console.log(res);
        })
        .catch(error => {
            console.log(error);
        });

        //Retrieve skills for a particular profile on database
        axios.default.request({
            method: 'GET',
            url: 'http://localhost:3001/Skills'
        })
        .then(res => {
            this.setState({skillDB:res.data});
            console.log(res);
        })
        .catch(error => {
            console.log(error);
        });

        axios.default.request({
            method: 'GET',
            url: 'http://localhost:3001/Common/GetExperienceLevels'
        })
        .then(res => {
            this.setState({experienceDB:res.data});
            console.log(res);
        })
        .catch(error => {
            console.log(error);
        });
    }

    public updateCheckBox (event:any) {
        const idCheck = event.target.id;
        const valueCheck = event.target.value;
        this.state.locationDB.find(i => i.name == idCheck ).checked = event.target.checked ? 1 : 0;
    }

    public updateSkills (item: skillItem) {
        let skillArr = this.state.skillDB;
        const objIndex = skillArr.findIndex(i => i.value == item.value );
        skillArr[objIndex] = item;
        this.setState({skillDB: skillArr})
    }

    public handleStateChange (event:any): void{
        let stateObject = event.target.id;
        this.setState({[stateObject as any] : event.target.value});
    }

    public addArray (event:any) {
        let skilArr = this.state.skillDB;
        let lastValue = (skilArr.length > 0) ? skilArr[skilArr.length - 1].value : 0;
        let newSkill = {
            name : "",
            value : lastValue + 1,
            year : 0,
            expId : 0
        }
        skilArr.push(newSkill);
        this.setState({skillDB: skilArr});
    }

    public deleteArray (event:any) {
        const skillArr = this.state.skillDB;
        let delIndex = skillArr.findIndex(x=>x.value == event.target.value);
        skillArr.splice(delIndex,1);
        this.setState({skillDB:skillArr});
        // arr.splice(arr.indexOf(+event.target.value), 1);
        // this.setState({skills:arr});
    }

    public handleSubmit (event:any): void{
        axios.default.post('http://localhost:3001/Location',
            JSON.stringify(this.state)
        )
        .then(function (response) {
            console.log(response.data.Result);
        })
        .catch(function (error) {
            console.log(error);
        });
    }
    public generateDataOject () {
        return {
            skills : this.state.skillDB,
            experienceLevels : this.state.experienceDB
        }
    }

    public render() {
        const dispCheckBoxes = this.state.locationDB.map((disp:dispItem) =>
            <LocationCheckbox key={disp.name} action={this.updateCheckBox} item={disp}/>
        );
        const skillDB = this.state.skillDB;
        const expDB = this.state.experienceDB;

        return (<form className="form-group">
            <h1>Perfil</h1>
            <div>
                <div>
                    <div>
                        <label className={style.label}>Nome</label><br/>
                        <input value = {this.state.name} onChange={ e => this.handleStateChange(e) } type="text"  id="name" name="name" placeholder="Nome" />
                    </div>
                    <div>
                        <label className={style.label}>Email</label><br/>
                        <input onChange={ e => this.handleStateChange(e) } type="text" id="email"  name="lastName" placeholder="Last Name"/>
                    </div>
                    <div>
                        <label className={style.label}>Contacto</label><br/>
                        <input onChange={ e => this.handleStateChange(e) } type="text"  id="contact" name="contact" placeholder="Contacto"/>
                    </div>
                    <div>
                        <label className={style.label}>Expectativa Salarial</label><br/>
                        <input onChange={ e => this.handleStateChange(e) } type="text" id="expectedWage" name="expectedWage" placeholder="Expectativa Salarial"/>
                    </div>
                </div>
                <div className="AreaField">
                    <label className={style.label}>Descrição</label><br/>
                    <textarea onChange={ e => this.handleStateChange(e) } name="description" id="description" placeholder="Descricao" >
                    </textarea>
                </div>
                <h1> Disponibilidade Geografica </h1>
                <div>
                    {dispCheckBoxes}
                </div>
                <h1> Competencias </h1>
            <div>
                <SkillForms delete={this.deleteArray} add={this.addArray} update={this.updateSkills} skills={skillDB} experienceLevels={expDB}/>
            </div>
            <div>
            </div>
            <footer>
                <input type="button" onClick = { e => this.handleSubmit(e)} value="Submit"/>
            </footer>
        </div>

        </form>);
    }
}

export default PdcForm;